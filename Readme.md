# Just some SlackBuilds

I tweaked most of them, none written from scratch. If you find them useful, yay!

The Trezor-Bridge SlackBuild is trash, need to add:
	- User and group for trezor to run daemon
	- Add daemon in rc.d scripts
	- Add checks in SlackBuild to check for user and group
But as of now, it works, just run trezord when needed and kill it when done.

I **DO NOT** intend to update the SlackBuilds for version updates but I intend to 
update them when there is a major change to the build process or other build changes.

- **zRam** : https://github.com/otzy007/enable-zRam-in-Slackware
